# Driver's Location
![K-16.png](https://bitbucket.org/repo/jbB69A/images/939657766-K-16.png)

# UEFI Driver Atrtributes
- UEFI 드라이버는 protocol을 제공할 수 있고 쓸수 있는 UEFI loader에 의해 load되는 system driven image이다.
- specific hardware를 지원할 수 있고 현존하는 드라이버를 override할 수 있다.
- 다음과 같은 4가지 특성을 가지고 있다.  
  1. Supportive : UEFI drivers는 복잡한 bus hierachies를 가진다. UEFI 드라이버는 bus로 device를 연결시키기 위해 bus interface와 통신한다.  
  2. Independent : UEFI drivers는 독립적인 platform 저장 장치이다.   
  3. Flexible : UEFI driver는 UEFI Driver Binding Protocol을 만든다. 이는 UEFI 시스템에 유연성을 제공하는 특별한 UEFI-defined 프로토콜이다.  
  4. Extensible : UEFI driver는 확장가능하고 future bus와 device타입에 따라 확장할 수 있다.  

# UEFI Driver's Functions
- 펌웨어 확장
- 플랫폼에 국한되지 않는 범용성
- 빠른 개발 생산성
![K-17.png](https://bitbucket.org/repo/jbB69A/images/1794268010-K-17.png)
- 초기화된 Protocol and Functionality 와 Protocol and Functionality awaiting 초기화 사이의  다리 역할을 함.

# UEFI Driver's Contents
- Entry Point - 시스템 관리자가 UEFI 드라이버에 첫번째 지점
- Pubhlished Functions - 드라이버와 시스템을 개발하기 위한 함수
- Consumed Functions - PCI, I/O 디바이스에 접근하기 위한 함수 
- Data Structures - UEFI 드라이버가 필요로하는  변수

# UEFI Driver's Entry Point
![K-18.png](https://bitbucket.org/repo/jbB69A/images/1933753024-K-18.png)

# UEFI Drivers : Supported
![K-19.png](https://bitbucket.org/repo/jbB69A/images/647610665-K-19.png)

# Comparing Drivers and Applications
- 어플리케이션은 action, exits를 실행하고 궁극적으로 메모리로부터 제거된다. 반면에 드라이버는 시스템메모리에 유지된다.
![K-20.png](https://bitbucket.org/repo/jbB69A/images/321840628-K-20.png)

# UEFI Protocols
- A UEFI protocol is a block of function pointers and dat structures or APIs that have been defined by a specification.
- confused with Drivers : 프로토콜은 인터페이스이다. UEFI Driver는 실행 가능한 UEFI 이미지로 다양한 프로토콜들을 설치한다. UEFI Protocol은 function pointer들의 한 block이고 자료구조이고 스펙에 정의된 자료구조이다. 스펙은 GUID를 정의해야 한다.
- produced by Drivers :  UEFI driver는 부가적인 개인 데이터 필드를 유지할 수 있는 protocol 인터페이스를 제공한다. protocol 인터페이스 구조는 protocol function을 가리키는 포인터들을 가지고 있다.
- consumed by Anyone : UEFI protocol은 누구에게나 사용될 수 있다. 

# Example A : PCI I/O protocol
![K-21.png](https://bitbucket.org/repo/jbB69A/images/1408423629-K-21.png)
# Example B : Disk I/O protocol
![K-22.png](https://bitbucket.org/repo/jbB69A/images/351549419-K-22.png)
# Example C : Device Path protocol
![K-23.png](https://bitbucket.org/repo/jbB69A/images/925940285-K-23.png)

# Comparing Protocols and C++ class
![K-24.png](https://bitbucket.org/repo/jbB69A/images/1286413189-K-24.png)

# Driver Design Process
![K-25.png](https://bitbucket.org/repo/jbB69A/images/3600642589-K-25.png)

# UEFI Driver Types
![K-26.png](https://bitbucket.org/repo/jbB69A/images/2474144602-K-26.png)  

- Service Drivers  
1. 하드웨어를 관리하지 않는다.
2. 다른 드라이버에 서비스를 제공하지 않는다.
3. 프로토콜에 국한된 드라이버를 지원하지 않는다.  
4. 일반적으로 드라이버 엔트리포인트에서 프로토콜을 설치한다.  
5. 적어도 하나 이상의 서비스 핸들러를 만들어야 한다.  
6.  서비스 스펙에 맞춘 프로토콜을 만들고 서비스핸들러로 설치를 한다.

- 드라이버 초기화 Initializing Drivers  
1. 보통 하드웨어와 함께 만들어야 한다. 
2. 초기화 과정은 한번만 이루어져야 한다.  
3. handles를 생성하지 말아야 한다. 
4. 프로토콜을 만들지 않아야 한다. 
5. 초기화 과정이 끝날때 언로드 해야 한다.   

- Root Bridge Drivers  
1. Typically manages part of the core chipset  
2. Makes direct contact with the hardware  
3. Creates one or more root bridge handles  
4. Produces root bridge I/O protocols and is installed onto new root bridge handles  
 
- Bus Drivers  
1. Start() creates one or more child handles  
2. Start() produces bus-specific I/O protocols and is installed onto the bus's child handles  

- Hybrid Drivers  
1. Manages and enumerates a bus controller  
2. Start() creates one or more child handles  
3. Start() produces bus-specific I/O protocols and is installed onto the bus's controller handles and child handles  

- Device Drivers  
1. Manages a controller or peripheral device  
2. Start() does not create child handles  
3. Start() produces one or more I/O protocols and is installed onto the device's controller handle  

# Consumed Protocols
![K-27.png](https://bitbucket.org/repo/jbB69A/images/3347220725-K-27.png)

# Produced Protocols
- Keyboard :Simple input protocol
- Mouse :  Simple pointer protocol
- Hard drive, CD-ROM, USB, Floppy : Block I/O protocol
- SCSI Aapter : SCSI pass-through protocol for each of the adapter's channels(channel이 하나라면 하나의 protocol만 필요하지만 채널이 여러개라면 그 채널마다 protocol이 필요하다.)
- Network Interface Controller(NIC)  
다음 UEFI driver들 중 하나를 필요로 함  
1. Universial Network Driver Interface(UNDI) and Network Interface Identifier Protocol (Intel이 사용)  
2. Managed Network Protocol(MNP)  
3. Simple Network Protocol(SNP)  

# Writing UEFI drivers
![K-28.png](https://bitbucket.org/repo/jbB69A/images/2790848016-K-28.png)

# Driver Initialization
![K-29.png](https://bitbucket.org/repo/jbB69A/images/3994893689-K-29.png)

# Supported()
![K-30.png](https://bitbucket.org/repo/jbB69A/images/3534214820-K-30.png)
# Start()
![K-31.png](https://bitbucket.org/repo/jbB69A/images/851612678-K-31.png)
# Stop()
![K-32.png](https://bitbucket.org/repo/jbB69A/images/1820827216-K-32.png)

# Recommended Driver Protocols
![K-33.png](https://bitbucket.org/repo/jbB69A/images/2395617095-K-33.png)

# Driver Guidelines
- Don't touch hardware in Driver Entry
- Keep Supported() small and simple
- Move complex I/O into Start() and Stop()
- Start() and Stop() mirror each other and Driver Entry and Unload mirror each other

# Driver Design Checklist
![K-34.png](https://bitbucket.org/repo/jbB69A/images/3588583402-K-34.png)

# Implement, Test, and Debug
![K-35.png](https://bitbucket.org/repo/jbB69A/images/1414423893-K-35.png)

# Start() Code
![K-36.png](https://bitbucket.org/repo/jbB69A/images/2384412934-K-36.png)

# Stop() Code
![K-37.png](https://bitbucket.org/repo/jbB69A/images/1341728597-K-37.png)

# Implement, Test, and Debug
![K-35.png](https://bitbucket.org/repo/jbB69A/images/1414423893-K-35.png)

# Using UEFI Driver Library
![K-38.png](https://bitbucket.org/repo/jbB69A/images/2118226842-K-38.png)   

- EDK2 library : <http://sourceforge.net/projects/edk2/files/EDKII_Libraries/UDK2010.SR1/>  
- Windows : .CHM file - MdePkg Package Document with Libraries

# Other helpful Suggestion
![K-39.png](https://bitbucket.org/repo/jbB69A/images/3921669129-K-39.png)