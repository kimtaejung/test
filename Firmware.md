#Firmware Storage Defined
- Firmware storage는 BIOS 또는 FIrmware code 를 저장할 수 있는 비휘발성 메모리이다. UEFI의 이점중 하나는 DXE와 PEI CORE code들이 firmware storage부터 어떻게 code를 추출해 올지 안다는 것이다.  
![K-4.png](https://bitbucket.org/repo/jbB69A/images/4142553045-K-4.png)

# UEFI Firmware Files
- UEFI Firmware 파일은 Firmware Volume 안에 저장되어 있는 code와 data이다.
- Atrributes  
1. Name
2. Type
3. Alignment
4. Size
- Smallest and least modular code defined in the system.
- UEFI Build Tool은 Firmware file을 만든다.
- UEFI Firmware Files의 자료구조  
![K-2.png](https://bitbucket.org/repo/jbB69A/images/1157067282-K-2.png)
![K-3.png](https://bitbucket.org/repo/jbB69A/images/3495356601-K-3.png)

# Firmware File System
- FIrmware File System(FFS)는 FV내에 Free space와 파일의 조직을 나타낸다.
- 각각의 FFS는 새롭게 노출된 FV을 driver와 관련짓기 위해 사용되는 고유한 GUID를 가진다.
- 게다가, 각각의 file system header는 Firmware volume data를 조직화하는데 사용되는 Firmware file system의 포멧을 나타내는 GUID를 포함하고 있다.
- FFS는 FAT32 file system과 같은  방법론을 사용한다(FAT32 : A file system that describes files and directors on particular mass media.). 이 의미는 media로 부터 file을 얻어오기 위해서는 FAT32 file system을 참조해야 할 필요가 있다는 것이다. 마찬가지로 firmware flash 장치로부터 firmware file을 가져오려면 firmware file system을 참조해야 한다.  
![K-5.png](https://bitbucket.org/repo/jbB69A/images/1323839426-K-5.png)

#Firmware Volume
- Firmware Volume 은 논리적인 Firmware 장치이다. 각각의 Firmware volume은 파일 시스템으로 조직화 되어있다. 그리고 흔히 말하는 file이 firmware 저장의 기본 unit이다. 
- Firmware Volume은 물리 저장장치의 논리적인 representation을 나타낸다. FV는 어떻게 이 logical unit을 조직하느냐에 따라서 하나의 flash 또는 여러개의 flash 장치의 한 부분을 대표할 수 있다.  물론, you can also be other dispartitions or even over the network.
- FV는 어떤 특정한 장치에 명시되어 있지 않다.(Not device-specific)
- 모든 PI Component들은 FV에 존재한다.
- FV는 읽혀지기 위해 반드시 FFS를 사용해야한다.  
![K-6.png](https://bitbucket.org/repo/jbB69A/images/3722268995-K-6.png)

# Firmware Devices
- Firmware Devices는 비휘발성 저장 component이다.
- Firmware code, Firmware data, 그리고 Fault Tolerent등 다양한 분야로 나뉘어져 있다.
- device를 프로그램하는 자원들을 포함하고 있다.

# Inside Firmware Devices
- FV Main - contains DXE Phase drivers
- FV Recovery - used to store the security and PEI phase code.  
![K-7.png](https://bitbucket.org/repo/jbB69A/images/722735701-K-7.png)
![K-8.png](https://bitbucket.org/repo/jbB69A/images/763060734-K-8.png)
- Firmware recovery volume안에는 firmware file system들어있다. 그 firmware file system은 Boot block code(PEI and Security phase code를 포함)와 Recovery code(usb device를 부르는 usb stack들을 포함)가 들어있다.

# Creating UEFI Firmware Files
![K-10.png](https://bitbucket.org/repo/jbB69A/images/271238586-K-10.png)  

- PEI Phase를 명시된 uefi image type이 있다. 그것은 terse image라는 것이다. PEI phase에서 rom이나 flash device에서 실행을 해야하기 때문에 uefi image보다 더 작은 헤더를 사용해야 한다. 다음 그림과 같이 UEFI image는 더 큰 header를 사용하기 때문에 PEI phase에서 사용하려면 Terse image를 만들어야 한다.  

![K-12.png](https://bitbucket.org/repo/jbB69A/images/3113473985-K-12.png)
# MAP
![K-9.png](https://bitbucket.org/repo/jbB69A/images/2635108817-K-9.png)