#Boot Device Selection(BDS)
- Boot Device Selection(BDS)는 operating system(OS)를 어디에 어떻게 부트시킬지를 결정한다.  
![K-41.png](https://bitbucket.org/repo/jbB69A/images/3877624063-K-41.png)

#Primary Steps of the BDS
- Steps  
  1.Language 와 string 데이터베이스 초기화  
  2.디바이스 리스트 빌드  
  3.디바이스 연결  
  4.콘솔 디바이스 탐지  
  5.메모리 성능 테스트  
  6.프로세스 부트 옵션  

#BDS’ Goals
- 중앙 집중식 정책과 유저 인터페이스
- 최소한의 드라이버 초기화와 유저 인터렉션이 가능한 부팅
- 플랫폼 부팅 정책을 위한 중앙 저장소를 만든다.
- 메뉴 설정 구현을 허용

#BDS' Attributes and Functions
### BDS 단계는 다음과 같은 특별한 속성과 기능을 갖고 있다. 
- BDS AP : BDS Architectural Protocol(AP)는 BDS 단계에서 수행된다. 이 프로토콜은 DXE 단계에서 발견되고, 다음 두 조건에 만족할 대 수행된다.
1.APs 가 handle database에 등록 되어있다.
2.DXE Dispatcher가 그 DXE 와 UEFI드라이버의 리스트를 디스패치 했다.   
사실 많은 OEM 밴더들은 그들만의 구현환경을 위해 BDS 단계를 사용하지 않는다.
- Control : BDS 는 플랫폼 초기화중에 DXE Foundation에 대한 제어권을 갖고 있다가 부팅 대상에 그 제어권을 넘긴다. (HDD, CD/ROM, etc)
- Watchdog :  BDS는 watchdog timer로 boot failure를 막는다.
- User Interface/ Independent Hardware Vendors(IHV) : setup 메뉴를 사용자에게 제공한다. 이 단계가 진행되는 동안 셋업과 부트리스트의 유지및 관리, IHV 어댑터 설정, 진단, 복구등에 대한 부분을 조정할 수 있다. 그러나 Original Equipment Manufacturers(OEMs)은 주어진 시장에서 뭘 노출시킬지와 플램폿을 위한 비즈니스적 요구를 어떻게 충족 시킬지, 두가지 사이에서 선택해야 한다.
- Boot Targets : 부팅 실패나 사용자의 개입이 있을 경우, BDS 단계는 boot target list를 채운다. boot target(부팅 대상)들은 드라이버들과 함계 글로벌 변수 리스트에 저장된다. 그러나 DXE 단계에서 boot targets중 하나가 완전히 초기화되지 않거나 등록되지 않은경우, BDS는 boot targets을 초기화 하기 위해 DXE 서비스를 실행해야 한다.

#DXE-to-BDS Flow
![K-42.png](https://bitbucket.org/repo/jbB69A/images/1745457626-K-42.png)

#BDS Entry Point Code
![K-43.png](https://bitbucket.org/repo/jbB69A/images/819386701-K-43.png)

#Setup Menu
- PlatformBdsPolicyBehavior 는 set-up menu를 호출 하는 코드이다.

![K-44.png](https://bitbucket.org/repo/jbB69A/images/2578572815-K-44.png)

#BDS Boot Option Attributes
- BDS는 가능한 모든 부트 디바이스를 나열한다.
- 디바이스 경로를 생성한다.
- BDS는 모든 디바이스와 연결하고, 사용자가 Auto Boot를 인터럽트 할 때 그것을 열거한다.
- Boot Option을 열거하는 절차
1.Lagacy Boot를 위한 Lagacy boot option(enable 되어있을 때)
2.UEFI boot를 위한 UEFI Boot option

#EDK Boot manager
![K-45.png](https://bitbucket.org/repo/jbB69A/images/2255543809-K-45.png)

#“DXE Main Calls BDS” Code
![K-46.png](https://bitbucket.org/repo/jbB69A/images/911697934-K-46.png)

#Console Device
![K-47.png](https://bitbucket.org/repo/jbB69A/images/180800573-K-47.png)
- Input : ConIn은 디폴트 Input 디바이스의 디바이스 경로이다.
- Processing Manager :  BDS단계가 진행되는 동안 당신이 (Conin,Conout,errout등)에 접근 해야 할 때, EFI system table이 업데이트 되고 이 table은 ConIn과 ConOut, ErrOut을 가리킨다.
- Output : ConOut과 ErrOut 은 각각 디폴트 Output, ErrOutput 디바이스의 디바이스 경로이다.

#Boot option list
![K-50.png](https://bitbucket.org/repo/jbB69A/images/1511314867-K-50.png)
##Boot Option list Global Defined Variables
- BootOrder :  Boot#### 변수의 정렬된 리스트를 구성하는 UINT16의 리스트이다. BootOrder 는 선택적인 변수가 아니다.
- Boot#### : 부트 로드 옵션이다. BDS는 상술되어 있는 부트 드라이버를 로드하려 한다.
- BootNext : 오직 다음 부트를 위해서 사용되는 옵션이다. 이 옵션은 Boot#### 변수보다 우선시 된다.

#UEFI Device Path
![K-49.png](https://bitbucket.org/repo/jbB69A/images/2606591301-K-49.png)

#BDS Does what?
- UEFI 드라이버를 메모리에 이미징한다.
- UEFI 디바이스 경로는 부트 디바이스를 나타낸다.
- Console 디바이스 초기화
- 부트 디바이스 초기화
- OS 로더에게 제어권을 넘긴다.

#Defining HII
- human interface infrastructure(HII) 은 데이터베이스, 데이터구조, 브라우저 엔진 으로 구성되어 있다. 데이터 구조와 함수들의 프로시저로 정의된 UEFI는 플랫폼 펌웨어나 하나의 드라이버에  통합되어 있다. 그래서 개발자들은 하나의 통합된 유저 인터페이스인 HII를 사용해야 한다.  
- 기존 BIOS의 문제 해결
- 시스템 펌웨어는 일반 셋업 브라우저를 위한 HII 사용한다.
- HII 데이터베이스로부터의 데이터 사용이 가능하다.

#HII's goal
![K-52.png](https://bitbucket.org/repo/jbB69A/images/636336570-K-52.png)

#HII Communication Design
![K-53.png](https://bitbucket.org/repo/jbB69A/images/2306207374-K-53.png)

#HII Benefits for a User Interface
- HII를 사용함으로서 이런 화려한 UI를 가능하게 한다.

![K-54.png](https://bitbucket.org/repo/jbB69A/images/1256641089-K-54.png)

#HII Database Function
![K-55.png](https://bitbucket.org/repo/jbB69A/images/1252859985-K-55.png) 

#HII Components
- HII 데이터베이스에 이미지, 폼, 폰트, 문자열 등의 구성요소들이 있으며 드라이버에 의해 로딩되어 사용됨
- 문자열은 유니코드로 저장되는 것이 UEFI 표준이다.  빌드 툴은 드라이버에서 문자열을 추출한다. 그 후에 드라이버는  C 소스코드의 일부분에 문자열을 위한 토큰을 포함한다.
- 특수한(영어 아닌) 언어를 지원한다. 그러므로 너는 영어, 독어 등 너가 원하는 모든 언어에 연결 가능함.
- form은 UI 위에 위치한다. 매번 어플리케이션 혹은 드라이버는 그들의 초기화 과정중에 form을 설치한다. fonts, strings, images와 함께 HII 데이터베이스에 저장된다.
- 유저의 정보를 form에 입력하면 다른 어플리케이션이 그 정보에 접근이 가능하다. 
- form 브라우저는 HII 데이터베이스를 다루고 유저와 상호작용, 결과값을 저장한다는 점에서   UEFI 어플리케이션과 유사하다.
- package 는 하나의 드라이버 또는 드라이버들과 패키지의 fonts, strings, forms 을 포함하는 데이터구조를 스스로 지원한다.

#Visual Forms Representation
- VFR은  한 페이지 레이아웃에서 한 언어만 사용한다. VFR은 UEFI의 부분이 아니라 EDK 2 빌드 툴의 한 부분이라고 해야 맞다.

#Internal Forms Representation
- IFR은 UEFI 스펙에 설명되어 있다. EDK 2 빌드툴은 IFR안의 VFR을 준수한다. 

#Minimum files for HII
- HII를 개발하고자 할때 다음과 같은 파일을 포함시켜야 한다.

![K-56.png](https://bitbucket.org/repo/jbB69A/images/1004517593-K-56.png)

