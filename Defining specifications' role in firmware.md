## BIOS란?
컴퓨터를 부팅할 때, 하드웨어를 set up 하고 OS를 불러오는 역할을 한다.
## UEFI란?
UEFI는 LEGACY BIOS를 대체하기 위해 개발됨  
OS와 platform firmware(BIOS) 사이에 interface 역할을 함
## BIOS layer and UEFI layer
### - BIOS layer
firmware layer로 하드웨어에 관련된 모든것이 있고 ROM에 저장되어 있다.  
이 layer는 어떻게 하드웨어 layer가 동작하게 할 것인가와 하드웨어 장치의 수를 결정한다.
### - UEFI layer
UEFI layer에서는 다른 소프트웨어나 펌웨어의 요소가 요구하는 서비스나 프로토콜을 제공한다.
#Framework
## - Platform initialization(PI) and UEFI
UEFI는 OS와 system firmware 사이에 가교역할을 하는 programmatic interface역할밖에 못한다.  
PI는 power-on부터 os에 제어권을 넘겨주는것 까지 platform을 초기화 하는 전 범위의 operation을 포함한다.
## PI의 구조
![noname01.png](https://bitbucket.org/repo/jbB69A/images/1610771361-noname01.png)
![qwre.png](https://bitbucket.org/repo/jbB69A/images/3756900746-qwre.png)