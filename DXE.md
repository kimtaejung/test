#Driver execution environment(DXE)
- device들이 초기화되고 UEFI 서비스가 지원되고 프로토콜과 드라이버가 실행되는 phase이다. 또한 UEFI interface를 만드는 table도 제공된다.  
![www.png](https://bitbucket.org/repo/jbB69A/images/2916572719-www.png)
- DXE는 Green H의 중상부에 위치하고 여기서 메모리가 초기화되고 chipset 초기화가 일어난다.

#Importance of DXE
-  DXE는 부팅 과정에서 매우 중요한 역할을 한다. 다음은 부팅이 DXE phase가 있는 것과 없는 것이 어떻게 영향을 주는지 보여준다.
- Without DXE : DXE 없이는 BIOS와 Firmware 기능들이 독점방식으로 코딩 되는 경향이 있다. 이 의미는 BIOS 밴더들이 어떤 주어진 코드 베이스가 없이 코드를 짠다는 것이다. 예를들면, BIOS 밴더들이 새로운 기능을 porting을 할때 어려움을 겪을 수 있다. 왜냐하면 BIOS 벤더들끼리 같은기능을 계속 port할 수 있기 때문이다. 또한 하나의 소스 파일이 부트 흐름을 제어한다. 이것은 새로운 기능을 추가하기 어렵게 한다. DXE 없이 코드를 짜는 것은 code를 흐트러지게 하고 디버깅하기 힘들게 한다.
- With DXE : DXE가 있다면 코딩은 OS를 제공하는 회사의 코드와 같은 방식으로 짜여진다. OS를 제공하는 회사의 코드는 드라이버와 잘 정의된 라이브러리들이 있다. OS를 제공하는 회사들은 복잡한 코드의 양을 제한함으로써 개발자들에게 이해하기 쉽도록 코드를 만든다. 드라이버와 잘 짜여진 라이브러리들은 개발자가 더 모듈화된 코드를 짤수 있도록 해준다.

#DXE Foundation's functions
- chipset과 platform을 초기화한다. boot manager와 os를 load하는 환경을 만드는 드라이버를 불러온다.
- DXE와 UEFI드라이버를 dispatch한다.
- UEFI driver는 dependency rule이 없기 때문에 DXE driver 후에 dispatch된다. dependency rule은 없지만 implied rule은 있음(모든 boot service와 runtime service가 초기화 될것임)
- boot manager를 부르고 BDS phase로 제어권을 넘겨준다.

#DXE Foundation Properties
- DXE Foundation은 HOB list 정보를 받아 시작된다.
- hardware-specific에 대해서 생각할 필요가 없다. DXE Foundation은 hardware에 추상적으로 접근할 수 있는 Architecture Protocols(APs)를 사용한다.
- DXE Foundation을 system memory 어디서든 불러올 수 있다. hard-coded adress가 없기 떄문이다.

#DXE Components
- Foundation : 드라이버를 dispatch하고 boot service, runtime service, DXE service를 제공하는 main DXE executable binary이다.
- Drivers : processor, chipset and platform components를 초기화 한다. DXE 드라이버는 Architectur Protocols(APs)를 제공한다.
- Dispatcher : DXE Foundation의 한 부분으로 드라이버를 찾고 정확한 방법으로 실행한다.
- EFI System Table : UEFI service table, configuration table, handle databases, console devices를 가리키는 포인터가 있다.
- Architectural Protocols(APs) : DXE driver가 APs 를 하드웨어로부터 온 abstract DXE에게 제공한다. DXE Foundation만이 APs를 부를수 있다. APs는 프로세서처럼 모든 silicon architecture-things을 캡슐화한다.

#PEI to DXE Entry
![wwwqew.png](https://bitbucket.org/repo/jbB69A/images/1808719289-wwwqew.png)
![qqweqwe.png](https://bitbucket.org/repo/jbB69A/images/3590275037-qqweqwe.png)

#DXE PHASE Flow
![qwqwqqq.png](https://bitbucket.org/repo/jbB69A/images/1106802786-qwqwqqq.png)

#EFI System table
EFI system table은 시스템에 있는 모든 자료구조를 포인트하는 포인터이다.  
![K-1.png](https://bitbucket.org/repo/jbB69A/images/1470156788-K-1.png)

#DXE Foundation Data Structures  
![K-2.png](https://bitbucket.org/repo/jbB69A/images/3333587591-K-2.png)

#Events
- Event는 프로토콜처럼 다른 오브젝트들에게 신호를 보내는 messaging method이다.  
![K-3.png](https://bitbucket.org/repo/jbB69A/images/2188635096-K-3.png)  
- Wait Events : event가 chekced되거나 기다릴때(wait) 마다 실행되는 알림 function.
- Exit Boot Services Events : UEFI boot service인 ExitBootServices()를 call하면 waiting state 로 부터 signaled state로  바뀌는 signal event.  
- Set Virtual Adress Map Events : UEFI Runtime service인 SetVirtualAdressMap()을 call 하면 위와 마찬가지로 signaled state로 바뀐다.
- Periodic Timer Events : specified frequency 에서 signaled state로 바뀌는 timer event
- One Shot Timer Events : specified timer period가 지나간 뒤 signaled state로 바뀌는 timer event.

# DXE Foundation code flow
- DXE Foundation code flow에는 다음과 같은 두가지 특성이 있다.  
![K-4.png](https://bitbucket.org/repo/jbB69A/images/3484919148-K-4.png)
# DXE Main code location
- \MdeModulePkg\Core\Dxe\DxeMain\DxeMain.c에 위치함  
![K-5.png](https://bitbucket.org/repo/jbB69A/images/1292511376-K-5.png)

# Architectural Protocols
- Function : AP는 platform-specific hardware를 격리하는 wrapper function의 역할을 한다. 예를 들면, CPU AP는 interrupt를 관리하고 프로세서 정보를 검색하고 프로세서 기반 타이머를 query(질문)한다.
- Support : AP는 하위레벨 프로토컬로 boot service와 runtime service를 지원한다. AP는 상위 레벨 platform function에 supplying call을 보냄으로써 지원을 한다.
- Dependency : dependency load order를 control하기 위해 다음과 같은 방식중에 하나를 사용해야 한다.  
  1. DXE를 load하기 위해 정확한 Dpendency Grammar  
  2. AP가 load되었을때 알리기 위한 RegistryProtocolNotify()  
  3. A Priori List - firmware volume 안에 GUID의 파일이름 리스트가 저장되어 있는 file  

# AP's role
![K-7.png](https://bitbucket.org/repo/jbB69A/images/2830706145-K-7.png)

# DXE AP's locations
![K-8.png](https://bitbucket.org/repo/jbB69A/images/2396611955-K-8.png)

# DXE Dispatcher
- DXE와 UEFI drivers를 블러오고 dispatch함.
- Driver는 다른 organizations, divisions, companies로 쓰여야 하기 때문에, DXE dispatcher는 DXE driver간에 실행명령을 해결한다.
- supports known business issues, including standardized methods for emergency patches. Additionally, the DXE Dispatcher makes it easier for developers to integrate their ddrivers.
-  THe DXE Dispatcher has expansion hooks incorporated in its design, with the intention of integrating more functionality. for example, security

# DXE dispatcher driver Entry Point
- \MdeModulePkg\Core\Dxe\Image\Image.c에서 볼 수 있다.  
![K-9.png](https://bitbucket.org/repo/jbB69A/images/4106532755-K-9.png)

# DXE Dispatcher state Machine
![K-11.png](https://bitbucket.org/repo/jbB69A/images/3495608596-K-11.png)

# DXE Driver Types
- DXE drivers는 device 또는 service code를 포함한 modules이며 UEFI drivers 전에 실행된다.  
![K-12.png](https://bitbucket.org/repo/jbB69A/images/52365372-K-12.png)

# Boot Device Selction(BDS) Driver
- DXE가 마지막으로 부르는 driver
- consoles(keyboard, video)을 establish하고 UEFI boot 옵션을 처리한다.  
![K-13.png](https://bitbucket.org/repo/jbB69A/images/3111280119-K-13.png)

# System Management Mode Services
- SMM services are a high priority System Management Interrupt (SMI) that takes control of the system, including during runtime.  
![K-14.png](https://bitbucket.org/repo/jbB69A/images/203653154-K-14.png)

# System Management Mode Flow
![K-15.png](https://bitbucket.org/repo/jbB69A/images/1542510485-K-15.png)